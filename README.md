# Informatik_Projekt: 
# Projektziel:
Ein 2d, Top down, Dungeon, RPG, Rouglike Game.
D.h. dass man sich in einer 2d Welt von oben herabblickend durch verschiedene Räume mit Gegnern kämpft und bessere Waffen, Rüstung und Power Ups bekommt. Man steuert ein Dreieck welches der von der Blickrichtung her der Maus folgt was es einem ermöglicht besser zu zielen. Die vorher genannten Power Ups können schnelleres schießen oder laufen sein. Möglich wäre es auch eine Möglichkeit zum dashen einzubringen, kurze Unsterblichkeit, oder ähnliches. Die Gegner sollen eine andere Geometrische Form, vorraussichtlich ein Quadrat bekommen. Es soll wenn möglich unendlich lange gehen und je weiter man kommt nur schwerer werden und falls es genügend Zeit gibt einen Boss einzubauen.
# Arbeit am Projekt
## 14.12.2023
### Ziele: 
Gitea Raster kopieren, Gitea Projektziel schreiben, bisschen Tipp 10, schießen anfangen
### Erreichte Ziele
Gitea Raster kopieren, Gitea Projektziel schreiben, bisschen Tipp 10, schießen, Dashen angefangen
### Probleme

### Notizen
Dashen nur bei 2 Tasten angefangen
### Quellen
https://scratch.mit.edu/projects/11317273/editor/   -->  Geguckt wie man Mausklick erkennt und es an die Kugel sendet und das Klone sich nicht selber klonen
## 21.12.2023
### Ziele:
Dashen beenden, Random Chest drop anfangen
### Erreichte Ziele
Dashen, Chest öffnen möglich und sie kommt wieder
### Probleme
Es kommt noch nix aus der Kiste
### Notizen
Nächstes mal Gadgets/Upgrades wie: Schnelleres dashen, Mehr Schaden, Unsterblichkeit (kurzzeitig)
### Quellen
https://undermine.fandom.com/wiki/Thorium_Chest --> Kisten Bild
##  11.01.2024
### Ziele:
Gadgets und dass sie aus der Kiste kommen
### Erreichte Ziele
Dash kommt aus kiste zuerst aber Gegner bevor ich mehr Gadgets mache damit ich sie testen kann. Gegner ,,Block" fehlt nur noch der Schaden.
### Probleme

### Notizen
Health bar machen
### Quellen

## 22.01.2024
### Ziele:
Schaden und Leben
### Erreichte Ziele
Schaden und Leben für Spieler und Gegner man kann nun sterben
### Probleme

### Notizen

### Quellen
https://www.pngfind.com/mpng/boowwJ_health-bar-sprite-sheet-parallel-hd-png-download/
## 23.01.2024
### Ziele:
Schnelleres Schießen Gadget und Randomiser
### Erreichte Ziele
Schnelleres Schießen Gadget und Randomiser
### Probleme

### Notizen
Scratch geht liste durch mit zb 50/50 Chance auf Schießen wenn nicht dann checkt 50/50 auf Dash und geht weiter die Liste durch bis etwas zutrifft, d.h. 
anfangs kleine Chancen machen und nach unten hin erhöhen
### Quellen

## 24.01.2024
### Ziele:
Besserer Kistenrandomiser und noch ein Gadget
### Erreichte Ziele
Kistenrandomiser geht zwar liste durch mit verschiedenen chancen aber und fängt neu an wenn nix zutrifft anstatt nichts/das letzte nochmal zu geben 
Health Gadget eingebracht welches 1 Leben gibt wenn es eingesammelt wird
### Probleme

### Notizen
Als nächstes neue gegner?
### Quellen

## 25.01.2024
### Ziele:
Neuen Gegner wenn fertig dann Schrotflinte einbauen
### Erreichte Ziele
Neuer Miniboss
### Probleme
Kugeln wollen bei seinem tot nicht immer weggehen
### Notizen

### Quellen

## 01.02.2024
### Ziele:
Gadgets für Kugeln die um den Spieler fliegen und Schaden machen. Sie sollen verschiedene Elemente mit Effekten haben   
### Erreichte Ziele
Die Gadgets gestylet aber es funktioniert noch nicht 
### Probleme
Ich sehe keine möglichkeit es zu machen und werde es wahrscheinlich abbrechen
### Notizen

### Quellen

## 15.02.2024 
### Ziele: 
Die Gadgets vom letzten mal überarbeiten
### Erreichte Ziele
Die Gadgets vom letzten mal überarbeiten
### Probleme

### Notizen
Sie drehen jetzt nicht mehr um den Spieler sondern fliegen neben ihm her. Nicht genug Gegner. Neuen nächstes mal machen.
### Quellen

## 19.02.2024
### Ziele:
Spawner Gegner machen
### Erreichte Ziele
Spawner Gegner machen, Level machen (es können noch mehr machen), Feuer Ball gelöscht und dafür eine möglichkeit für 4 Eisbälle gegeben,
Wahrscheinlichkeiten der Gadgets angepasst, Man kann per Portale zum nächsten Level kommen, Hintergrund ändert sich ein wenig (wir in wirklichkeit nur gedreht)
### Probleme

### Notizen
Es ist schwer vielleich etwas einfacher machen und mehr Level welche dann schwer sind
### Quellen
https://gifer.com/en/4xjg
https://benmusick.artstation.com/projects/xGr14
## 20.02.2024
### Ziele:
Level mehr und dafür einfacher machen um mehr Kisten im Spieleverlauf zu bekommen, ansonsten was spontan einfällt
### Erreichte Ziele
Level angepasst -> geht bis 20, Codes die durchgehend an waren größtenteils anders kaufen lassen um Lag zu reduzieren, Timer gemacht der auch super funktioniert und am Ende nach dem das Spiel endet gezeigt wird, Extra Leben (Schild) hinzugefügt damit Health Gadget was bei vollen Leben bringt
### Probleme
So viel Code und Skripts das Scratch teilweise nicht mehr flüssig läuft, deswegen beende ich es größtenteils bei diesem Code-Stand. 
### Notizen

### Quellen

## 29.02.2024
### Ziele: 
Letzte Feinschliffe am Spiel. Es ist fertig aber noch kleine aber feine Sachen einfügen wenn ich noch ne Idee bekomme. 
### Erreichte Ziele
Beim betreten eines Levels kommt kurz ein Portal welches den Spieler hinteleportiert, Bug Fixes x3
### Probleme

### Notizen

### Quellen

## 
### Ziele:

### Erreichte Ziele

### Probleme

### Notizen

### Quellen

